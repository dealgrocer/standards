## CSS Patterns for BeautyMNL CSS2.0
A reference guide on building a more modular css system for BeautyMNL. **It's a work in progress**
 
### Table of Contents
#### Variables
  * [Colors](#markdown-header-colors)
  * [Typography](#markdown-header-typography)
#### Utilities
  * [Spacing](#markdown-header-spacing)
  * [Widths](#markdown-header-widths)
  * [Text Helpers](#markdown-header-text-helpers)
  * [Grid Items](#markdown-header-grid-items)
#### Layout
  * [BMNL Header Bar](#markdown-header-bmnl-header-bar)
  * [Section](#markdown-header-section)
  * [Section Title](#markdown-header-section-title)
  * [Promo Boxes](#markdown-header-promo-boxes)
  * [Media Item](#markdown-header-media-item)
#### Components
  * [Accordion](#markdown-header-accordion)
  * [Accordion - radio](#markdown-header-accordion-radio)
  * [Tabs](#markdown-header-tabs)
  * [Breadcrumb](#markdown-header-breadcrumb)
  * [Dropdown](#markdown-header-dropdown)
  * [Card](#markdown-header-card)
  * [Card - Product](#markdown-header-product-card)
  * [Card - Article](#markdown-header-product-article)
  * [Taxon Box](#markdown-header-taxon-box)
  * [Taxon Pill](#markdown-header-taxon-pill)
  * [Taxon Image](#markdown-header-taxon-image)
  * [Panel](#markdown-header-panel)
#### Elements
  * [Button](#markdown-header-button)
  * [Alert](#markdown-header-alert)
  * [Tag](#markdown-header-tag)

# Grid Items
***
A basic inline-block grid system for products and articles.
```
.grid-items
  .grid-item
```
### Modifiers
Add a modifier to `.grid-items` to specify a 3,4 or 5 column layout for products or articles. Basically divide 100% by the number of columns you want.

#### 3-Column Layout
```
<div class=“grid-items grid-items-3”>
  <div class=“grid-item”>Add as many as you want</div>
</div>
```
#### 4-Column Layout
```
<div class=“grid-items grid-items-4”>
  <div class=“grid-item”>Add as many as you want</div>
</div>
```
#### 5-Column Layout
```
<div class=“grid-items grid-items-5”>
  <div class=“grid-item”>Add as many as you want</div>
</div>
```
### Flex
Any `.grid-item` would flex to the width of the container.
```
<div class=“grid-items grid-items-flex”>
  <div class=“grid-item”>Add as many as you want</div>
</div>
```
*Ok, let's stick with 5 for now*

### On mobile
You don't have to add modifiers, since `.grid-item` automatically divides itself by 2.

> Do not apply `.grid-items` to horizontal scrolling



# Colors
***
Pattern library for colors

#### Colors
```
$white: #ffffff;
$red: #db1c25;
$green: #37AF64;
$black: #101228;
$pink: #FAD0CD;
$power-pink: #E96483;
$gold: #b29d74;
$dark-gold: #987a4d;
$darker-gray: #777E86;
$gray: #9B9B9B;
$lighter-gray: lighten($gray, 20%);
$lightest-gray: lighten($gray, 33%);
```
#### Status Colors
```
$color-success: #64DEAD;
$color-error: #EC5E66;
$color-highlight: #FFFBE5;
```
> Note: Don't use `$red` for error messages. Only use `$color-error`.
 

# Typography
***
Variables for setting typography
```
$base-font-size: 14px;
$base-line-height: 1.5;

$small-font-size: 12px;
$large-font-size: 16px;
$xlarge-font-size: 18px;

$h-font-size: 20px;
$xh-font-size: 24px;
$xxh-font-size: 28px;
$xxxh-font-size: 34px;
```
### Fonts
I didn't change how the old mixins for including fonts.
> Refer to `stylesheets/base/_fonts.scss`


# Text Helpers
***
You can use classes to modify different text sizes.

#### Style
```
.is-link
```
#### Position
```
.is-text-right
.is-text-center
```
#### Size
```
.is-text-large
.is-text-xlarge
.is-text-h
.is-text-xh
.is-text-xxh
.is-text-xxxh
```
#### Color
```
.is-text-primary
.is-text-danger
.is-text-success
```

### Clipping Text
When you don't want text to wrap and use ellipsis.
`.is-text-clipped`

# Spacing
***
A utility library for spacing. Default values are set on `rem` units.
> Overusing these classes might result in inconsistencies across different pages.
## Margin / Padding
Due to the ubiquitous nature of setting margin, these utilities use a shorthand naming convention.
#### Shorthand
m - margin

p - padding

t - top

r - right

b - bottom

l - left

x - x-axis (left and right)

y - y-axis (top and bottom)

#### Values
0 - 0 reset

1 - $space-1 (.5rem)

2 - $space-2 (1rem)

3 - $space-3 (2rem)

4 - $space-4 (4rem)


#### Example
`.px1` Writes as `padding-left: .5rem; padding-right: .5rem`

`.mb4` Writes as `margin-bottom: 4rem;`


# Widths
***
A small utility library based on a 12-column grid for setting widths.
**For example:**

```
<div class="u-width-7">width: 58.33333% <div>
```

# Section
***
A simple container to divide a page into sections. Sections can have a title and a sub title.
**`.section`** class can be used in any context, but mostly as a block to separate sections.

### Section Title
It's always recommended to add a title to every section.
**`.section-title`** Title of the section

### Modifiers
```
  .section-title.is-large
  .section-title.is-larger
  .section-title.is-headline
  .section-title.section-title__post-title // For Article title
```
> Adding a `.button` inside the `.section-title` automatically floats to right. This is especially useful for positioning the See All button

### Sub Title
**`.sub-title`** A secondary text right after the section title

### Section Slug
**`.section-slug`** is placed above the `.section-title` to indicate a parent. 
For example:
```
<a class="section-slug">Skin care</a>
<div class="section-title section-title__post-title">5 Skin Problems and the Soaps That Fix Them</div>
``` 

 
# BMNL Header Bar
***
This is the main `position: fixed` header of BeautyMNL, contains the logo, links and checkout button.

Start with **`.header-bar`** as parent

**`.header-bar__brand`** Contains the logo

**`.header-bar__start`** Position to the left side

**`.header-bar__end`** Position to the right side

**`.header-item`** Each single item of the nav bar, this could be a link or may contain a button or a div

```
.header-bar
  .header-bar__brand
  .header-bar__start
    .header-bar__item 
  .header-bar__end
    .header-bar__item
```

Add `.header-bar`**`.is-static`** to remove the fixed position.



# Promo Boxes
***
This is used as a container for the promo boxes for the site. 

```
.promo-boxes // Container for the promo box
  .promo-box
    .promo-box__header This is optional if you want to add a title for the promo box
    .promo-box__content
      .image // This would contain the image for the marketing ad to make it adapt with the width
```
> You can add as many `.promo-box` as you need, use sparingly.

### For mobile
Add a modifier **`.is-horizontal`** to enable horizontal scrolling for mobile for 2 or more promo boxes.
```
.promo-boxes.is-horizontal
  %div // You need this div to make horizontal scrolling on mobile
    .promo-box
```


# Breadcrumb
***
A simple breadcrumb (separated by slashes) to improve navigation experience
```
<nav class="breadcrumb">
  <ul>
    <li><a>All Beauty</a></li>
    <li><a>Skin Care</a></li>
    <li><a>Lips</a></li>
  </ul>
</nav>
```


# Button
***
The button is an essential element of any design.  A button is meant to look and behave as an interactive element of your page.

`<a class="button">Button</a>`

**Things to note:**
 * Default button style is a bordered button
 * Button font is set to gotham-medium

### Modifiers

#### Color
**`.is-primary`**
 
**`.is-secondary`**
 
**`.is-white`**
 
 
#### Size
**`.is-small`**
 
**`.is-large`**
 
**`.is-slim`**

**`.is-fullwidth`**

#### Icon
```
<a class="button has-icon">
  <i class="icon"></i>
  Button
</a>
```
#### Disabled State
> Use the html attribute **`disabled`** instead. This enables a full disabled state rather than just adding a class that users can still interact.


# Alert
***
A highlighted notification to alert users of something

`.alert` Default alert component, default color is used for errors

### Modifiers
#### Colors
`.is-success`

`.is-notice`

`.is-dark`


# Tabs
***
A responsive horizontal tabs.

```
<div class="tabs">
  <ul>
    <li class="active"><a>What to Love</a></li>
    <li><a>How to Use</a></li>
    <li><a>About the Brand</a></li>
  </ul>
</div>
<div class="tab-content">Content goes here...</div>
```
### Modifiers
#### Size

**`.tabs.is-large`**

**`.tabs.is-fullwidth`** Equally spaces the tabs with the width of the container


# Media Item
***
A ui element for presenting any short content with images.

```
<div class="media-item">
  <div class="media-left"></div>
  <div class="media-content">
    Content goes here...
    <div class="media-action"><a class="button"></a></div> // Optional if media items require a button element
  </div>
</div>
```
### Modifiers
#### Size
`.media-left.is-small` for smaller images


# Dropdown
***
An interactive ui dropdown menu for selecting content.
```
<div class="dropdown">
  <button class="button">
    <span>Popularity</span>
  </button>
  <div class="dropdown-menu">
    <div class="dropdown-content">
      <label>Sort by</label>
      <a class="dropdown-item">Popularity</a>
      <a class="dropdown-item">Average Rating</a>
    </div>
  </div>
</div>
```
### Modifiers
#### Position
```
<div class="dropdown is-right"></div>
```


# Accordion
***
A simple accordion that utilizes checkbox to toggle content
```
<div class=“accordion”>
  <ul>
    <li>
      <input type=“checkbox”>
      <a class=“accordion-title”>About</a>
      <div class=“accordion-content”>Content here</div>
    </li>
    <li>
      <input type=“checkbox”>
      <a class=“accordion-title”>How it Works</a>
      <div class=“accordion-content”>Content here</div>
    </li>
  </ul>
</div>
```

# Accordion - radio
***
An accordion that shows radio buttons that can be selected and toggle content one at a time
```
<div class=“radio-accordion”>
  <label class=“radio-accordion__toggle”>
    <input type=“radio”>
    This is a label
    <div class=“radio-accordion__content”>
      Content here
    </div>
  </label>
</div>
```

# Card
***
A flexible and composable component that contains 1 image and a content
```
<div class="card">
  <div class="card-image"></div>
  <div class="card-content">
    <div class="card-content__info> //Optional Info (e.g. locations, variants) </div>
    Content here...
  </div>
</div>
```
### Modifiers
`.card.is-row` transforms into a media type layout

There are ways to use a `.card` for products and articles

## Product Card
***
This is an example of using a `.card` for Products.
```
.card
 .card-image
 .card-content
   %a.product-title Eyebrow Gel
   %a.brand Nyx
   .price-container
     %span.product-card__price P360
     %span.price-before-sale P460
     .stars
       .star ...
```
### Modifiers
#### Sold out
`.card.is-soldout`
> Append a text `Sold Out` after the `.stars` selector

## Article Card
***
This is an example of using a `.card` for Articles.
```
.card
  .card-image
  .card-content
    %a.article-tag
    %h3.article-title The 5 Wellness Trends You Need to Know Now
```

### Horizontal scrolling
On mobile, follow the markup below:
```
.product-horizontal-scrolling
  %div
    .grid-item
      .card
```



# Taxon Box
***
A taxon component that contains two product images and has a label. This is used to link to an inner taxon (e.g. Face, Body, etc).
```
%a.taxon-box
  .group // clears the two thumbnails inside
    %img{ src }
    %img{ src }
  .taxon-box__label  Face
```

# Taxon Pill
***
A taxon component similar to `.taxon-box` but only contains one image similar to a [Media Item](#markdown-header-media-item). This is mostly used as a link to the last child of the taxon.
```
%a.taxon-pill
  .taxon-pill__image %img{ src }
  .taxon-pill__label Cleansers
```


# Taxon Image
***
A taxon component that has a label and a corresponding image. There are two types of Taxon image - an overlay text or a block text.
#### Overlay
```
%a.taxon-image
 %img{src}
 .taxon-image__overlay
   %h3.taxon-image__label Skin Care
```
#### Block Label
```
%a.taxon-image.is-bordered
  %img{src}
  %h3.taxon-image__label Laser Hair Removal
```
To clip the label to two lines, add a `span` tag.
```
%h3.taxon-image__label
  %span Laser Hair Removal
```
### Horizontal scrolling
On mobile, follow the markup below:
```
.taxon-horizontal-scroller
 %div
  .grid-item
    %a.taxon-image
     ...
```

# Panel
***
A stackable panel

```
<div class="panel">
  <div class="panel-item">A non-link panel-item</div>
  <a class="panel-item">A link panel-item</a>
<div>
```
### Modifiers
**`.is-selected`**
`<div class="panel-item is-selected"></div>`

# Tag
***
An element for a tag label
`<span class="tag">Invoiced</span>`

### Modifiers
```
<span class="tag is-success">Success<span>
<span class="tag is-notice">Notice<span>
<span class="tag is-dark">Dark<span>
<span class="tag is-gold">Gold<span>
<span class="tag is-danger">Danger<span>
```